#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Artgen.py -- simple art generator.

to try it, run:

>python Artgen.py

major revisions:
2021/07/07 - lnt - first. generates a single color jpeg with user-defined size.
"""
import os
import numpy as np
from pathlib import Path
from random import randint
from PIL import Image, ImageDraw

# Script working directory
swd = Path(__file__).resolve().parent

################################################################################
# Settings dictionary
################################################################################

s = {
    'w'     : 4,                        # image width  / inches
    'h'     : 4,                        # image height / inches
    'dpi'   : 200,                      # dots per inch
    'w_k'   : 0.5,                      # background weighting
    'nk'    : 9,                        # number of neighbors considered
    'canvas': 'rand',                   # type of canvas or background
    'output_Path' : swd / 'testdata'    # data save path (object)
}

################################################################################
# Test suite
################################################################################

def test1():
    """Simple unit test."""
    g = Artgen(**s)
    g.make_image()
    g.nn_process()
    g.add_frame(10, (254,100,100))
    g.save_image('test1.jpg')
    g.show()

def test2():
    """Simple test with another drawing example."""
    g = Artgen(**s)
    g.make_image()
    g.set_focal_point()
    g.add_wandering_circle()
    # g.add_frame(5, (91, 68, 62))
    g.save_image('test2.png')
    g.show()

################################################################################
# Art-generation class
################################################################################

class Artgen:
    """Generates art."""

    def __init__(self, *args,
                 w=4,
                 h=4,
                 dpi=200,
                 w_k=0.8,
                 nk=3,
                 canvas = 'rand',
                 output_Path = './',
                 **kwargs):
        """Create mesh dimensions and initializes image object."""
        self.w              = w
        self.h              = h
        self.dpi            = dpi
        self.w_k            = w_k
        self.nk             = nk
        self.dim            = (w*dpi, h*dpi)
        self.canvas         = canvas
        self.output_Path    = output_Path

    def make_image(self):
        """Generates an image based on the canvas defined in the settings"""

        if self.canvas == 'rand':
            self.make_rand_image()

        elif self.canvas == 'white':
            self.make_white_image()

    def make_rand_image(self):
        """Generates an image based on random numbers"""
        # the array is a float -- it's kind of in "idea space" where everything is possible
        self.array  = np.random.randint(0, 255, size=(*self.dim, 3)).astype(float)

        # the image is a uint8 array. It's actually in the data type of the output file
        self.image = Image.fromarray(self.array.astype(np.uint8))

    def make_white_image(self):
        """Generates an all white image (like a blank canvas)"""
        # the array is a float -- it's kind of in "idea space" where everything is possible
        self.array  = np.ones((*self.dim, 3)).astype(float) * 255

        # the image is a uint8 array. It's actually in the data type of the output file
        self.image = Image.fromarray(self.array.astype(np.uint8))

    def add_frame(self, percent_width, color):
        """Adds a single-color frame to the generated image"""
        # convert the latest image to an array
        self.array = np.array(self.image)

        # calculate the frame width
        w_frame = int(self.w * self.dpi * percent_width / 100)

        # create a frame
        self.array[:w_frame,:,:]    = color
        self.array[:,:w_frame,:]    = color
        self.array[-w_frame:,:,:]   = color
        self.array[:,-w_frame:,:]   = color

        # add some noise (the frame is its own work of art)
        self.array  += np.random.randint(-5, 5, size=(*self.dim, 3)).astype(np.uint8)

        # convert the array back to an image object
        self.image = Image.fromarray(self.array.astype(np.uint8))

    def nn_process(self):
        """Processes nearest-neighbors"""

        # pre-weight array
        self.array *= (1 - self.w_k)

        a_k = self.w_k/((self.nk + 1)**2 - 1)

        for k in range(1, self.nk+1):
            self.array[k:, :, :] += a_k * self.array[:-k, :, :]
            self.array[:, k:, :] += a_k * self.array[:, :-k, :]

        self.image = Image.fromarray(self.array.astype(np.uint8))

    def set_focal_point(self):
        """pick a point in the image to be more interesting"""
        im_center = (tuple(map(lambda p: p/2, self.image.size)))        
        im_std = (tuple(map(lambda p: p/4, self.image.size)))
        self.focal_point = tuple(np.random.normal(im_center, im_std, 2))
        return None

    def add_wandering_circle(self):
        """Add a colorful circle that wanders around."""
        circle_center = self.focal_point
        for _ in range(10):
            self.add_circle(circle_center)
            circle_center = tuple(map(lambda p: p + np.random.normal(0, 40), circle_center))
        return None

    def add_circle(self, circle_center):
        """Overlay a circle on the image."""
        circle_corners = self.get_circle_corners(circle_center)
        draw = ImageDraw.Draw(self.image)
        color = tuple(np.random.choice(range(256), size=3))
        draw.ellipse(circle_corners, fill=color, outline='black')
        return None

    def get_circle_corners(self, circle_center):
        """Define a bounding box for a circle"""
        typical_radius = min(self.image.size)/30
        radius = np.random.normal(typical_radius, typical_radius/2)
        near_corner = tuple(map(lambda p: round(p) - radius, circle_center))
        far_corner = tuple(map(lambda p: round(p) + radius, circle_center))
        return (near_corner, far_corner)

    def save_image(self, fname):
        """Save the image."""
        # create output path if it doesn't exist
        self.output_Path.mkdir(parents=True, exist_ok=True)

        self.image.save(self.output_Path / fname)

    def show(self):
        """Shows the image"""
        self.image.show()


################################################################################
# Script main point-of-entry
################################################################################

if __name__ == "__main__":
    test1()
    test2()
